package com.spartons.androidml.fragments.selection

@FunctionalInterface
interface SelectionAdapterActionHandler {

    fun itemSelected(item: SelectType)
}
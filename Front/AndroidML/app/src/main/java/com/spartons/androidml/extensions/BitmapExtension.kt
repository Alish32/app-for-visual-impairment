package com.spartons.androidml.extensions

import android.graphics.Bitmap


operator fun Bitmap.component1() = this.width

operator fun Bitmap.component2() = this.height
package com.spartons.androidml

import android.app.Application
import com.google.firebase.FirebaseApp

class MyApplication : Application() {
    companion object {
        var globalVar = 0
        var globalVar2 = ""
        var baseURL = "http://e461639c8e1a.ngrok.io"
    }

    override fun onCreate() {
        super.onCreate()
        FirebaseApp.initializeApp(this)
    }
}
package com.spartons.androidml.fragments.emotionDetection

fun Int.toDetectionType(): DetectionType {
    return when (this) {
        1 -> DetectionType.EMOTION_DETECTION
        2 -> DetectionType.COLOR_DETECTION
        3 -> DetectionType.TEXT_DETECTION
        4 -> DetectionType.OBJECT_DETECTION
        5 -> DetectionType.IMAGE_DETECTION
        6 -> DetectionType.CURRENCY_DETECTION
        else -> throw IllegalArgumentException("Invalid $this value for DetectionType")
    }
}

enum class DetectionType(val value: Int) {
    EMOTION_DETECTION(1),
    COLOR_DETECTION(2),
    TEXT_DETECTION(3),
    OBJECT_DETECTION(4),
    IMAGE_DETECTION(5),
    CURRENCY_DETECTION(6)
}
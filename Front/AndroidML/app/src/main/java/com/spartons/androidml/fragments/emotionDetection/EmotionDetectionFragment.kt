package com.spartons.androidml.fragments.emotionDetection

import android.R
import android.annotation.SuppressLint
import android.graphics.Bitmap
import android.graphics.Color
import android.graphics.ImageFormat
import android.graphics.Matrix
import android.hardware.camera2.CameraCaptureSession
import android.hardware.camera2.CameraCharacteristics
import android.hardware.camera2.CameraDevice
import android.hardware.camera2.CameraManager
import android.media.*
import android.net.Uri
import android.os.Bundle
import android.os.Handler
import android.os.HandlerThread
import android.speech.tts.TextToSpeech
import android.util.Log
import android.view.*
import androidx.fragment.app.Fragment
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import androidx.palette.graphics.Palette
import com.android.volley.Request
import com.android.volley.Response
import com.android.volley.toolbox.JsonObjectRequest
import com.google.firebase.ml.vision.FirebaseVision
import com.google.firebase.ml.vision.automl.FirebaseAutoMLLocalModel
import com.google.firebase.ml.vision.face.FirebaseVisionFaceDetectorOptions
import com.google.firebase.ml.vision.label.FirebaseVisionOnDeviceAutoMLImageLabelerOptions
import com.google.firebase.ml.vision.label.FirebaseVisionOnDeviceImageLabelerOptions
import com.spartons.androidml.MyApplication
import com.spartons.androidml.cameraUtil.AutoFitSurfaceView
import com.spartons.androidml.cameraUtil.OrientationLiveData
import com.spartons.androidml.cameraUtil.getPreviewOutputSize
import com.spartons.androidml.extensions.*
import com.spartons.androidml.fragments.emotionDetection.DetectionType.*
import com.spartons.androidml.utils.ColorUtil
import com.spartons.androidml.utils.MySingleton
import kotlinx.coroutines.*
import org.json.JSONException
import org.json.JSONObject
import java.io.ByteArrayOutputStream
import java.io.File
import java.util.*
import java.util.concurrent.atomic.AtomicInteger
import kotlin.collections.component1
import kotlin.collections.component2
import kotlin.collections.component3
import kotlin.coroutines.resume
import kotlin.coroutines.resumeWithException
import kotlin.coroutines.suspendCoroutine
import kotlin.math.log


class EmotionDetectionFragment : Fragment() {

    private companion object {
        private val firebaseImageRotations =
            mapOf((0 to 0), (90 to 1), (180 to 2), (270 to 3))
        private const val CAMERA_ID = "0"
        private const val IMAGE_TAKE_LATENCY = 3000L
        private const val IMAGE_DETECTION_OBJECT_CONFIDENCE = 0.6
    }
    private val currencyMap = mapOf("5euro" to "5 Euro", "10euro" to "10 Euro", "20euro" to "20 Euro", "50euro" to "50 Euro", "100euro" to "100 Euro", "200euro" to "200 Euro", "500euro" to "500 Euro")

    private val args: EmotionDetectionFragmentArgs by navArgs()

    private val atomicInteger = AtomicInteger()
    private val textToSpeechCallback = TextToSpeech.OnInitListener {}

    private val colorUtil by lazy { ColorUtil() }
    private val mySingleton by lazy { MySingleton(this.context) }

    private val textToSpeech by lazy {
        TextToSpeech(requireContext(), textToSpeechCallback).apply { language = Locale.US }
    }

    private val firebaseVision by lazy {
        FirebaseVision.getInstance()
    }

    private val firebaseTextDetector by lazy {
        firebaseVision.onDeviceTextRecognizer
    }

    private val firebaseVisionDetector by lazy {
        val firebaseVisionsFaceDetectorOptions = FirebaseVisionFaceDetectorOptions.Builder()
            .setClassificationMode(FirebaseVisionFaceDetectorOptions.ALL_CLASSIFICATIONS)
            .setPerformanceMode(FirebaseVisionFaceDetectorOptions.FAST)
            .build()
        firebaseVision.getVisionFaceDetector(firebaseVisionsFaceDetectorOptions)
    }

    private val objectDetector by lazy {
        val firebaseVisionOnDeviceImageLabeler = FirebaseVisionOnDeviceImageLabelerOptions.Builder()
            .setConfidenceThreshold(0.7f)
            .build()
        firebaseVision.getOnDeviceImageLabeler(firebaseVisionOnDeviceImageLabeler)
    }

    private val currencyOnDeviceAutoMLImageLabeler by lazy {
        val localModel = FirebaseAutoMLLocalModel.Builder()
            .setAssetFilePath("manifest.json")
            .build()
        val options = FirebaseVisionOnDeviceAutoMLImageLabelerOptions.Builder(localModel)
            .setConfidenceThreshold(0.5f)
            .build()
        firebaseVision.getOnDeviceAutoMLImageLabeler(options)
    }

    private val onDeviceAutoMLImageLabeler by lazy {
        val localModel = FirebaseAutoMLLocalModel.Builder()
            .setAssetFilePath("manifest.json")
            .build()
        val options = FirebaseVisionOnDeviceAutoMLImageLabelerOptions.Builder(localModel)
            .setConfidenceThreshold(0.8f)
            .build()
        firebaseVision.getOnDeviceAutoMLImageLabeler(options)
    }

    private val autoSurfaceView by lazy {
        AutoFitSurfaceView(requireContext())
    }

    private val characteristics by lazy {
        requireContext().cameraManager.getCameraCharacteristics(CAMERA_ID)
    }

    private val imageReader by lazy {
        val pixelFormat = ImageFormat.JPEG
        val size = characteristics.get(CameraCharacteristics.SCALER_STREAM_CONFIGURATION_MAP)
            ?.getOutputSizes(pixelFormat)
            ?.maxBy { it.height * it.width }!!
        ImageReader.newInstance(size.width, size.height, pixelFormat, 1)
    }

    private val cameraThread = HandlerThread("CameraThread").apply { start() }

    private val cameraHandler = Handler(cameraThread.looper)

    private val imageReaderThread = HandlerThread("imageReaderThread").apply { start() }

    private val imageReaderHandler = Handler(imageReaderThread.looper)

    var take = true

    private val surfaceCallback = object : SurfaceHolder.Callback {
        override fun surfaceChanged(holder: SurfaceHolder?, format: Int, width: Int, height: Int) =
            Unit

        override fun surfaceDestroyed(holder: SurfaceHolder?) = Unit

        override fun surfaceCreated(holder: SurfaceHolder?) {
            val previewSize = getPreviewOutputSize(
                autoSurfaceView.display, characteristics,
                SurfaceHolder::class.java
            )
            autoSurfaceView.holder.setFixedSize(previewSize.width, previewSize.height)
            autoSurfaceView.setAspectRatio(previewSize.width, previewSize.height)
            autoSurfaceView.post { initializeCamera() }
        }
    }

    private var camera: CameraDevice? = null
    private var session: CameraCaptureSession? = null
    private var relativeOrientation: OrientationLiveData? = null
    private var previousView: View? = null

    @SuppressLint("MissingPermission")
    private suspend fun openCamera(manager: CameraManager, cameraId: String, handler: Handler) =
        suspendCancellableCoroutine<CameraDevice> { cont ->
            manager.openCamera(cameraId, object : CameraDevice.StateCallback() {
                override fun onOpened(camera: CameraDevice) {
                    cont.resume(camera)
                }

                override fun onDisconnected(camera: CameraDevice) {
                    requireContext().toast("Camera $cameraId has been disconnected!")
                    findNavController().navigateUp()
                }

                override fun onError(camera: CameraDevice, error: Int) {
                    val msg = when (error) {
                        ERROR_CAMERA_DEVICE -> "Fatal (device)"
                        ERROR_CAMERA_DISABLED -> "Device policy"
                        ERROR_CAMERA_IN_USE -> "Camera in use"
                        ERROR_CAMERA_SERVICE -> "Fatal (service)"
                        ERROR_MAX_CAMERAS_IN_USE -> "Maximum cameras in use"
                        else -> "Unknown"
                    }
                    val exception = RuntimeException(msg)
                    if (cont.isActive) cont.resumeWithException(exception)
                }
            }, handler)
        }

    private fun initializeCamera() = lifecycleScope.launch {
        openCamera(requireContext().cameraManager, CAMERA_ID, cameraHandler).apply {
            val surface = autoSurfaceView.holder.surface
            session =
                createCaptureSession(this, listOf(surface, imageReader.surface), cameraHandler)
            val captureRequest =
                createCaptureRequest(CameraDevice.TEMPLATE_PREVIEW).apply { addTarget(surface) }
            session?.setRepeatingRequest(captureRequest.build(), null, cameraHandler)
        }.also { camera = it }
    }

    private suspend fun createCaptureSession(
        camera: CameraDevice,
        targets: List<Surface>,
        handler: Handler
    ): CameraCaptureSession = suspendCoroutine { cont ->
        camera.createCaptureSession(targets, object : CameraCaptureSession.StateCallback() {
            override fun onConfigureFailed(session: CameraCaptureSession) {
                val exception =
                    RuntimeException("Camera ${camera.id} session configuration failed!")
                cont.resumeWithException(exception)
            }

            override fun onConfigured(session: CameraCaptureSession) {
                cont.resume(session)
            }
        }, handler)
    }

    private suspend fun takePhoto() = suspendCancellableCoroutine<Pair<Int, Image>> { cont ->

        @Suppress("ControlFlowWithEmptyBody")
        while (imageReader.acquireNextImage() != null) {
        }

        imageReader.setOnImageAvailableListener({ reader ->
            val rotation = relativeOrientation?.value ?: 0
            val firebaseRotation = firebaseImageRotations[rotation] ?: 0
            reader.setOnImageAvailableListener(null, null)
            cont.resume(firebaseRotation to reader.acquireLatestImage())
        }, imageReaderHandler)

        runCatching {
            session?.device?.createCaptureRequest(
                CameraDevice.TEMPLATE_STILL_CAPTURE
            )?.apply { addTarget(imageReader.surface) }?.also {
                session?.capture(it.build(), null, cameraHandler)
            }
        }
    }

    private suspend fun detectImageFromFirebaseML(image: Image, orientation: Int) {
        val visionImage = image.toFirebaseVisionImage(orientation)
        try {
            val visions = firebaseVisionDetector.detectInImage(visionImage).await()
            if (visions.isEmpty()) {
                speakText("No face detected")
                return
            }
            for (visionFace in visions) {
                val smileProbability = visionFace.smilingProbability
                if (smileProbability < 0.1)
                    speakText("Neutral face")
                else if (smileProbability >= 0.1 && smileProbability < 0.8)
                    speakText("Happy face")
                else speakText("Person is smiling")
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    private suspend fun speakText(msg: String, id: Int? = null) = withContext(Dispatchers.Main) {
        val utteranceId = id ?: atomicInteger.incrementAndGet()
        textToSpeech.speak(
            msg, TextToSpeech.QUEUE_ADD,
            null, utteranceId.toString()
        )
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?
    ): View {
        mTTS = TextToSpeech(this.context, TextToSpeech.OnInitListener { status ->
            if (status != TextToSpeech.ERROR) {
                //if there is no error then set language
                mTTS.language = Locale.US
            }
        })
        previousView?.let { return it }
        autoSurfaceView.holder.addCallback(surfaceCallback)
        relativeOrientation = OrientationLiveData(requireContext(), characteristics).apply {
            observe(viewLifecycleOwner, androidx.lifecycle.Observer { })
        }
        return autoSurfaceView.also { previousView = it }
    }

    override fun onResume() {
        super.onResume()
        lifecycleScope.launch(Dispatchers.IO) {
            // for to remove the latency in first speak
            speakText("")

            while (isActive) {
                delay(IMAGE_TAKE_LATENCY)
                // stop to take photo while the text is speaking
                if (textToSpeech.isSpeaking) continue
                takePhoto().let {
                    it.second.use { image ->
                        when (args.detectionType.toDetectionType()) {
                            EMOTION_DETECTION -> detectImageFromFirebaseML(image, it.first)
                            COLOR_DETECTION -> detectMostDominantsImageColor(image)
                            TEXT_DETECTION -> detectTextFromFirebaseML(image, it.first)
                            OBJECT_DETECTION -> detectObjectInImage(image, it.first)
                            IMAGE_DETECTION -> detectPersonInImage(image, it.first)
                            CURRENCY_DETECTION -> detectCurrencyInImage(image, it.first)
                        }
                    }
                }
            }
        }
    }

    lateinit var mTTS: TextToSpeech

    private suspend fun detectPersonInImage(image: Image, orientation: Int) {
        if(take) {
            take = false
            save_and_delete_unknown_face_image(image, orientation)
        }
    }

    private fun recognizePerson(){
        val params = JSONObject()

        try {
            params.put("name", "check")
        } catch (e: JSONException) {
            e.printStackTrace()
        }
        val url = MyApplication.baseURL + "/rec/run_paralel/"
        val jsonObjectRequest = JsonObjectRequest(Request.Method.POST, url, params,
            Response.Listener { response ->
                take = true
                Log.d("aaaa", response.getString("res"))
                mTTS.speak(response.getString("res"), TextToSpeech.QUEUE_FLUSH, null)

            },
            Response.ErrorListener { error ->
                Log.d("aaaa", error.toString())

            }
        )
// Access the RequestQueue through your singleton class.
        MySingleton.getInstance(this.context).addToRequestQueue(jsonObjectRequest)
    }

    fun convertBitmapToByteArray(bitmap: Bitmap, orientation: Int): ByteArray? {

        val matrix = Matrix()
        var x:Int = 90
        matrix.postRotate(x.toFloat())

        val scaledBitmap = Bitmap.createScaledBitmap(bitmap, bitmap.width/30, bitmap.width/30, true)

        val rotatedBitmap = Bitmap.createBitmap(
            scaledBitmap,
            0,
            0,
            scaledBitmap.width,
            scaledBitmap.height,
            matrix,
            true
        )
        val stream = ByteArrayOutputStream()
        rotatedBitmap.compress(Bitmap.CompressFormat.PNG, 100, stream)
        return stream.toByteArray()
    }

    private fun save_and_delete_unknown_face_image(image: Image, orientation: Int){
        val params = JSONObject()
        var bitma = convertBitmapToByteArray(image.bitmap, orientation)
        val base64: String = android.util.Base64.encodeToString(bitma, android.util.Base64.DEFAULT)

        try {
            params.put("name", "check")
            params.put("img_data", base64)

        } catch (e: JSONException) {
            e.printStackTrace()
        }
        val url = MyApplication.baseURL + "/rec/save_and_delete_unknown_face_image/"
        val jsonObjectRequest = JsonObjectRequest(Request.Method.POST, url, params,
            Response.Listener { response ->
                recognizePerson()
            },
            Response.ErrorListener { error ->

            }
        )
        MySingleton.getInstance(this.context).addToRequestQueue(jsonObjectRequest)
    }

    private suspend fun detectCurrencyInImage(image: Image, orientation: Int) {
        val visionImage = image.toFirebaseVisionImage(orientation)
        try {
            val labels = currencyOnDeviceAutoMLImageLabeler.processImage(visionImage).await()
            var flag = false
            for (label in labels) {
                val currencyName = currencyMap[label.text] ?: ""
                if (currencyName.isNotEmpty()) {
                    speakText(currencyName)
                    flag = true
                }
            }
            if (flag.not())
                speakText("No currency detected")
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    private suspend fun detectObjectInImage(image: Image, orientation: Int) {
        val visionImage = image.toFirebaseVisionImage(orientation)
        val detections = objectDetector.processImage(visionImage).await()
        val flagList = mutableListOf<Boolean>()
        for (detection in detections) {
            if (detection.confidence >= IMAGE_DETECTION_OBJECT_CONFIDENCE) {
                Log.d("Label", detection.confidence.toString())
                Log.d("Confidence", detection.text)
                flagList.add(true)
                speakText(detection.text)
            } else flagList.add(false)
        }
        if (flagList.all { it.not() })
            speakText("I am not sure")
    }

    private suspend fun detectTextFromFirebaseML(image: Image, orientation: Int) {
        val visionImage = image.toFirebaseVisionImage(orientation)
        val visionText = firebaseTextDetector.processImage(visionImage).await()
        Log.d("aaa", visionText.text)
        speakText(visionText.text)
    }

    private suspend fun detectMostDominantsImageColor(image: Image) {
        var bitmap = image.bitmap
        bitmap = Bitmap.createBitmap(
            bitmap,
            bitmap.width / 3,
            bitmap.height / 3,
            bitmap.width / 3,
            bitmap.height / 5
        );
        runCatching {
            Palette.from(bitmap).generate()
        }.onSuccess {
            val rgb = it.getDominantColor(Color.BLACK)
            if (rgb == Color.BLACK) {
                speakText("Black")
                return
            }
            val (red, green, blue) = rgb.splitRgbValue()
            val colorName = colorUtil.getColorNameFromRgb(red, green, blue)
            speakText("$colorName")
        }.onFailure { speakText("No most dominant color found") }
    }

    override fun onStop() {
        super.onStop()
        runCatching { camera?.close() }
        lifecycleScope.coroutineContext.cancelChildren()
        textToSpeech.speak(
            "", TextToSpeech.QUEUE_FLUSH,
            null, atomicInteger.incrementAndGet().toString()
        )
    }

    override fun onDestroy() {
        super.onDestroy()
        cameraThread.quitSafely()
        imageReaderThread.quitSafely()
        firebaseVisionDetector.close()
        objectDetector.close()
        onDeviceAutoMLImageLabeler.close()
        currencyOnDeviceAutoMLImageLabeler.close()
        firebaseTextDetector.close()
        textToSpeech.shutdown()
    }
}

package com.spartons.androidml.fragments.selection

import com.spartons.androidml.fragments.emotionDetection.DetectionType

fun SelectType.toDetectionType(): DetectionType {

    return when (this) {
        SelectType.TEXT_TO_SPEECH -> DetectionType.TEXT_DETECTION
        SelectType.COLOR_DETECTION -> DetectionType.COLOR_DETECTION
        SelectType.EMOTION_DETECTION -> DetectionType.EMOTION_DETECTION
        SelectType.OBJECT_DETECTION -> DetectionType.OBJECT_DETECTION
        SelectType.IMAGE_DETECTION -> DetectionType.IMAGE_DETECTION
        else -> throw IllegalArgumentException("invalid $this type found for DetectionType")
    }
}

enum class SelectType(val value: String) {
    LIGHT_DETECTION("Light Detection"),
    EMOTION_DETECTION("Emotion Detection"),
    TEXT_TO_SPEECH("Text to Speech"),
    COLOR_DETECTION("Color Detection"),
    OBJECT_DETECTION("Object Detection"),
    MOTION_DETECTION("Motion Detection"),
    IMAGE_DETECTION("Image Detection")
}
package com.spartons.androidml

import android.Manifest
import android.annotation.SuppressLint
import android.app.Activity
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.graphics.Matrix
import android.os.Build
import android.os.Bundle
import android.os.Vibrator
import android.provider.MediaStore
import android.speech.tts.TextToSpeech
import android.util.Log
import android.view.Gravity
import android.view.MotionEvent
import android.view.View
import android.widget.*
import androidx.annotation.IntegerRes
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.isVisible
import androidx.core.widget.addTextChangedListener
import androidx.navigation.NavController
import androidx.navigation.findNavController
import com.android.volley.Request
import com.android.volley.Response
import com.android.volley.toolbox.JsonObjectRequest
import com.google.firebase.FirebaseApp
import com.spartons.androidml.extensions.bitmap
import com.spartons.androidml.extensions.isHasPermission
import com.spartons.androidml.fragments.emotionDetection.DetectionType
import com.spartons.androidml.utils.MySingleton
import org.json.JSONException
import org.json.JSONObject
import java.io.ByteArrayOutputStream
import java.util.*
import kotlin.concurrent.schedule


private const val PERMISSIONS_REQUEST_CODE = 10
private const val PERMISSIONS_REQUIRED = Manifest.permission.CAMERA
lateinit var radioGroup: RadioGroup
lateinit var button: Button
lateinit var button2: Button
lateinit var editTextTextPersonName: EditText

class MainActivity : AppCompatActivity() {

    private companion object {
        /** Combination of all flags required to put activity into immersive mode */
        const val FLAGS_FULLSCREEN =
            View.SYSTEM_UI_FLAG_LOW_PROFILE or
                    View.SYSTEM_UI_FLAG_FULLSCREEN or
                    View.SYSTEM_UI_FLAG_LAYOUT_STABLE or
                    View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY

        /** Milliseconds used for UI animations */
        private const val IMMERSIVE_FLAG_TIMEOUT = 2000L
    }

    lateinit var mTTS: TextToSpeech
    var slience = false


    //var x1: Float? = 0.toFloat()
    // var x2: Float? = 0.toFloat()
    var velocityX1: Float? = 0.toFloat()
    var velocityX2: Float? = 0.toFloat()
    private var flingCount = 0
    var itemSelected: Int = 1

    @SuppressLint("ResourceType")

    private var x1: Float = 0f
    private var x2: kotlin.Float = 0f
    val MIN_DISTANCE = 150

    //variable for counting two successive up-down events
    var clickCount = 0

    //variable for storing the time of first click
    var startTime: Long = 0

    //variable for calculating the total time
    var duration: Long = 0

    //constant for defining the time duration between the click that can be considered as double-tap
    val MAX_DURATION = 500

    @SuppressLint("ResourceType")
    override fun onTouchEvent(event: MotionEvent?): Boolean {
        when (event!!.action) {

            MotionEvent.ACTION_DOWN -> {
                x1 = event.x
                startTime = System.currentTimeMillis();
            }
            MotionEvent.ACTION_UP -> {

                val time = System.currentTimeMillis() - startTime
                duration = duration + time
                if (duration <= MAX_DURATION) {
                    if (itemSelected == 3) {
                        textToSpeech()
                        mTTS.speak("Scanning again", TextToSpeech.QUEUE_FLUSH, null)
                    }
                    if (itemSelected == 7) {
                        personDetection()
                        mTTS.speak("Taking again", TextToSpeech.QUEUE_FLUSH, null)
                    }
                }
                duration = 0

                x2 = event.x
                val deltaX = x2 - x1
                if (Math.abs(deltaX) > MIN_DISTANCE) {
                    // Left to Right swipe action
                    if (x2 > x1) {
                        itemSelected--
                        if (itemSelected <= 0)
                            itemSelected = 7
                        //Log.d("aaa", itemSelected.toString())
                        when (itemSelected) {
                            1 -> {
                                radioGroup.check(2131230795)
                            }
                            2 -> {
                                radioGroup.check(2131230793)
                            }
                            3 -> {
                                radioGroup.check(2131230797)
                            }
                            4 -> {
                                radioGroup.check(2131230791)
                            }
                            5 -> {
                                radioGroup.check(2131230796)
                            }
                            6 -> {
                                radioGroup.check(2131230792)
                            }
                            7 -> {
                                radioGroup.check(2131230794)
                            }
                        }
                    } else {
                        itemSelected++
                        if (itemSelected >= 8)
                            itemSelected = 1
                        // Log.d("aaa", itemSelected.toString())
                        when (itemSelected) {
                            1 -> {
                                radioGroup.check(2131230795)
                            }
                            2 -> {
                                radioGroup.check(2131230793)
                            }
                            3 -> {
                                radioGroup.check(2131230797)
                            }
                            4 -> {
                                radioGroup.check(2131230791)
                            }
                            5 -> {
                                radioGroup.check(2131230796)
                            }
                            6 -> {
                                radioGroup.check(2131230792)
                            }
                            7 -> {
                                radioGroup.check(2131230794)
                            }
                        }
                    }
                } else {
                    // consider as something else - a screen tap for example
                }
            }
        }

        return super.onTouchEvent(event)
    }

    private lateinit var navController: NavController

    private lateinit var container: FrameLayout
    private lateinit var switch: Switch

    fun vibrate() {
        var vibrator = getSystemService(Context.VIBRATOR_SERVICE) as Vibrator
        vibrator.vibrate(5)
    }


    @IntegerRes
    private var lastSelectedItemId: Int? = null
    private var lastSelectedItemType: Int? = null
    fun currencyDetection() {
        MyApplication.globalVar = 0
        vibrate()
        if (!slience)
        mTTS.speak("Currency detection", TextToSpeech.QUEUE_FLUSH, null)
        if (isHasPermission(PERMISSIONS_REQUIRED)) {
            val bundle = Bundle()
            bundle.putInt("detection_type", DetectionType.CURRENCY_DETECTION.value)
            navController.navigate(R.id.emotion_detection_fragment, bundle)
        } else if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            lastSelectedItemId = R.id.emotion_detection_fragment
            lastSelectedItemType = DetectionType.CURRENCY_DETECTION.value
            requestPermissions(arrayOf(PERMISSIONS_REQUIRED), PERMISSIONS_REQUEST_CODE)
        }
    }

    fun lightDetection() {
        MyApplication.globalVar = 0
        vibrate()
        if (!slience)
        mTTS.speak("Light detection", TextToSpeech.QUEUE_FLUSH, null)
        navController.navigate(R.id.light_detection_fragment)
    }

    fun emotionDetection() {
        MyApplication.globalVar = 0
        vibrate()
        if (!slience)
        mTTS.speak("Emotion detection", TextToSpeech.QUEUE_FLUSH, null)
        if (isHasPermission(PERMISSIONS_REQUIRED)) {
            val bundle = Bundle()
            bundle.putInt("detection_type", DetectionType.EMOTION_DETECTION.value)
            navController.navigate(R.id.emotion_detection_fragment, bundle)
        } else if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            lastSelectedItemId = R.id.emotion_detection_fragment
            lastSelectedItemType = DetectionType.EMOTION_DETECTION.value
            requestPermissions(arrayOf(PERMISSIONS_REQUIRED), PERMISSIONS_REQUEST_CODE)
        }
    }

    fun textToSpeech() {
        MyApplication.globalVar = 0
        vibrate()
        if (!slience)
        mTTS.speak("Text to speech", TextToSpeech.QUEUE_FLUSH, null)
        if (isHasPermission(PERMISSIONS_REQUIRED)) {
            val bundle = Bundle()
            bundle.putInt("detection_type", DetectionType.TEXT_DETECTION.value)
            navController.navigate(R.id.emotion_detection_fragment, bundle)
        } else if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            lastSelectedItemId = R.id.emotion_detection_fragment
            lastSelectedItemType = DetectionType.TEXT_DETECTION.value
            requestPermissions(arrayOf(PERMISSIONS_REQUIRED), PERMISSIONS_REQUEST_CODE)
        }
    }

    fun colorDetection() {
        MyApplication.globalVar = 0
        vibrate()
        if (!slience)
        mTTS.speak("Color detection", TextToSpeech.QUEUE_FLUSH, null)
        if (isHasPermission(PERMISSIONS_REQUIRED)) {
            val bundle = Bundle()
            bundle.putInt("detection_type", DetectionType.COLOR_DETECTION.value)
            navController.navigate(R.id.emotion_detection_fragment, bundle)
        } else if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            lastSelectedItemId = R.id.emotion_detection_fragment
            lastSelectedItemType = DetectionType.COLOR_DETECTION.value
            requestPermissions(arrayOf(PERMISSIONS_REQUIRED), PERMISSIONS_REQUEST_CODE)
        }
    }

    fun objectDetection() {
        MyApplication.globalVar = 0
        vibrate()
        if (!slience)
        mTTS.speak("Object detection", TextToSpeech.QUEUE_FLUSH, null)
        if (isHasPermission(PERMISSIONS_REQUIRED)) {
            val bundle = Bundle()
            bundle.putInt("detection_type", DetectionType.OBJECT_DETECTION.value)
            navController.navigate(R.id.emotion_detection_fragment, bundle)
        } else if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            lastSelectedItemId = R.id.emotion_detection_fragment
            lastSelectedItemType = DetectionType.OBJECT_DETECTION.value
            requestPermissions(arrayOf(PERMISSIONS_REQUIRED), PERMISSIONS_REQUEST_CODE)
        }
    }

    fun motionDetection() {
        MyApplication.globalVar = 0
        vibrate()
        mTTS.speak("Motion detection", TextToSpeech.QUEUE_FLUSH, null)
        if (isHasPermission(PERMISSIONS_REQUIRED)) {
            navController.navigate(R.id.motion_detection_fragment)
        } else if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            lastSelectedItemId = R.id.motion_detection_fragment
            requestPermissions(arrayOf(PERMISSIONS_REQUIRED), PERMISSIONS_REQUEST_CODE)
        }
    }

    fun convertBitmapToByteArray(bitmap: Bitmap): ByteArray? {
        val matrix = Matrix()
        var x:Int = 90
        matrix.postRotate(x.toFloat())

        val scaledBitmap = Bitmap.createScaledBitmap(bitmap, bitmap.width/30, bitmap.height/30, true)

        val rotatedBitmap = Bitmap.createBitmap(
            scaledBitmap,
            0,
            0,
            scaledBitmap.width,
            scaledBitmap.height,
            matrix,
            true
        )

        val stream = ByteArrayOutputStream()
        rotatedBitmap.compress(Bitmap.CompressFormat.PNG, 100, stream)
        return stream.toByteArray()
    }

    fun personDetection() {
        if(!editTextTextPersonName.isVisible)
        button2.visibility = View.VISIBLE
        MyApplication.globalVar = 0
        vibrate()
        if (!slience)
        mTTS.speak("Person detection", TextToSpeech.QUEUE_FLUSH, null)
        if (isHasPermission(PERMISSIONS_REQUIRED)) {
            val bundle = Bundle()
            bundle.putInt("detection_type", DetectionType.IMAGE_DETECTION.value)
            navController.navigate(R.id.emotion_detection_fragment, bundle)
        } else if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            lastSelectedItemId = R.id.emotion_detection_fragment
            lastSelectedItemType = DetectionType.IMAGE_DETECTION.value
            requestPermissions(arrayOf(PERMISSIONS_REQUIRED), PERMISSIONS_REQUEST_CODE)
        }
    }

    @SuppressLint("ResourceType")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        container = findViewById(R.id.fragment_container)
        navController = findNavController(R.id.fragment_container)
        radioGroup = findViewById<RadioGroup>(R.id.features_group)
        button = findViewById<Button>(R.id.button)
        button2 = findViewById<Button>(R.id.button2)
        editTextTextPersonName = findViewById<EditText>(R.id.editTextTextPersonName)
        button.visibility = View.GONE
        button2.visibility = View.GONE
        editTextTextPersonName.visibility = View.GONE

        mTTS = TextToSpeech(applicationContext, TextToSpeech.OnInitListener { status ->
            if (status != TextToSpeech.ERROR) {
                //if there is no error then set language
                mTTS.language = Locale.US
            }
        })

        button.setOnClickListener(){
            Toast.makeText(this, "asasssa", 2000)
            val intent = Intent()
            intent.type = "image/*"
            intent.action = Intent.ACTION_GET_CONTENT
            startActivityForResult(Intent.createChooser(intent, "Select Picture"), 1000);
        }
        button2.setOnClickListener(){
            editTextTextPersonName.visibility = View.VISIBLE
            button2.visibility = View.GONE
        }
        editTextTextPersonName.addTextChangedListener { charSequence  ->
            if (editTextTextPersonName.text.length > 0)
                button.visibility = View.VISIBLE
            else
                button.visibility = View.GONE
        }
        radioGroup.setOnCheckedChangeListener { z, i ->
            // Log.d("aaa", radioGroup.checkedRadioButtonId.toString())
//            val timer = Timer("schedule", true);
//            timer.schedule(1000){
//                Log.d("aaa", "asas")
//            }
            button.visibility = View.GONE
            button2.visibility = View.GONE
            editTextTextPersonName.visibility = View.GONE
            when (i) {
                R.id.btn_light_dt -> {
                    lightDetection()
                    itemSelected = 1
                }
                R.id.btn_emotion_dt -> {
                    emotionDetection()
                    itemSelected = 2
                }
                R.id.btn_color_dt -> {
                    colorDetection()
                    itemSelected = 4
                }
                R.id.btn_currency_dt -> {
                    currencyDetection()
                    itemSelected = 6
                }
                R.id.btn_speech_txt -> {
                    textToSpeech()
                    itemSelected = 3
                }
                R.id.btn_object_dt -> {
                    objectDetection()
                    itemSelected = 5
                }
                R.id.btn_image_dt -> {
                    personDetection()
                    itemSelected = 7
                }
            }
        }
        FirebaseApp.initializeApp(this.applicationContext)
    }

    override fun onResume() {
        super.onResume()
        // Before setting full screen flags, we must wait a bit to let UI settle; otherwise, we may
        // be trying to set app to immersive mode before it's ready and the flags do not stick
        container.postDelayed({
            container.systemUiVisibility = FLAGS_FULLSCREEN
        }, IMMERSIVE_FLAG_TIMEOUT)
    }

    override fun onBackPressed() {
        super.onBackPressed()
        finish()
    }

    override fun onPause() {
        super.onPause()
        slience = true
        radioGroup.check(2131230795)
        itemSelected = 1
        slience = false
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == Activity.RESULT_OK && requestCode == 1000) {
            // I'M GETTING THE URI OF THE IMAGE AS DATA AND SETTING IT TO THE IMAGEVIEW
            val bitmap =
                MediaStore.Images.Media.getBitmap(this.contentResolver, data?.data)

            val params = JSONObject()
            var bitma = convertBitmapToByteArray(bitmap)
            val base64: String = android.util.Base64.encodeToString(bitma, android.util.Base64.DEFAULT)

            try {
                var name = editTextTextPersonName.text
                params.put("name", name)
                params.put("img_data", base64)

            } catch (e: JSONException) {
                e.printStackTrace()
            }
            val url = MyApplication.baseURL + "/rec/save_known_face_image/"
            val jsonObjectRequest = JsonObjectRequest(
                Request.Method.POST, url, params,
                Response.Listener { response ->
                    val myToast = Toast.makeText(applicationContext,"Image uphoaded", Toast.LENGTH_LONG)
                    myToast.show()
                },
                Response.ErrorListener { error ->
                    Log.d("aaa", error.toString())
                }
            )
            MySingleton.getInstance(this).addToRequestQueue(jsonObjectRequest)

        }
    }

    override fun onRequestPermissionsResult(
        requestCode: Int, permissions: Array<out String>, grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        if (requestCode == PERMISSIONS_REQUEST_CODE) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                val lastItem = lastSelectedItemId ?: return
                if (lastItem == R.id.motion_detection_fragment)
                    navController.navigate(lastItem)
                else if (lastItem == R.id.emotion_detection_fragment) {
                    val lastType = lastSelectedItemType ?: return
                    val bundle = Bundle()
                    bundle.putInt("detection_type", lastType)
                    navController.navigate(lastItem, bundle)
                }
            } else Toast.makeText(this, "Permission request denied", Toast.LENGTH_LONG).show()
        }
    }
}

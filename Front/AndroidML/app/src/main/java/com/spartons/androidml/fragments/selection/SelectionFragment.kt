package com.spartons.androidml.fragments.selection

import android.Manifest
import android.content.pm.PackageManager
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.core.view.updateMargins
import androidx.fragment.app.Fragment
import androidx.navigation.NavDirections
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.spartons.androidml.extensions.isHasPermission

private const val PERMISSIONS_REQUEST_CODE = 10
private const val PERMISSIONS_REQUIRED = Manifest.permission.CAMERA

class SelectionFragment : Fragment(), SelectionAdapterActionHandler {

    private var lastSelectedItem: SelectType? = null

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?
    ) = RecyclerView(requireContext()).apply {
        val layoutParams = RecyclerView.LayoutParams(
            RecyclerView.LayoutParams.MATCH_PARENT,
            RecyclerView.LayoutParams.MATCH_PARENT
        )
        layoutParams.updateMargins(top = 20)
        this.layoutParams = layoutParams
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        view as RecyclerView
        view.run {
            layoutManager = LinearLayoutManager(requireContext())
            setHasFixedSize(true)
            adapter = SelectionAdapter(this@SelectionFragment)
        }
    }

    override fun onRequestPermissionsResult(
        requestCode: Int, permissions: Array<String>, grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
//        if (requestCode == PERMISSIONS_REQUEST_CODE) {
//            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
//                val lastItem = lastSelectedItem ?: return
//                if (lastItem == SelectType.MOTION_DETECTION)
//                    navigate(SelectionFragmentDirections.actionToMotionDetection())
//                else navigate(SelectionFragmentDirections.actionToEmotionDetection(lastItem.toDetectionType().value))
//            } else Toast.makeText(context, "Permission request denied", Toast.LENGTH_LONG).show()
//        }
    }

    override fun itemSelected(item: SelectType) {
        lastSelectedItem = item
//        if (item == SelectType.LIGHT_DETECTION)
//            navigate(SelectionFragmentDirections.actionToLightDetection())
//        else if (item == SelectType.EMOTION_DETECTION || item == SelectType.COLOR_DETECTION ||
//            item == SelectType.TEXT_TO_SPEECH || item == SelectType.OBJECT_DETECTION || item == SelectType.IMAGE_DETECTION) {
//            if (requireContext().isHasPermission(PERMISSIONS_REQUIRED)) {
//                navigate(SelectionFragmentDirections.actionToEmotionDetection(item.toDetectionType().value))
//            } else requestPermissions(arrayOf(PERMISSIONS_REQUIRED), PERMISSIONS_REQUEST_CODE)
//        } else if (item == SelectType.MOTION_DETECTION) {
//            if (requireContext().isHasPermission(PERMISSIONS_REQUIRED))
//                navigate(SelectionFragmentDirections.actionToMotionDetection())
//            else requestPermissions(arrayOf(PERMISSIONS_REQUIRED), PERMISSIONS_REQUEST_CODE)
//        }
    }

    private fun navigate(navDirections: NavDirections) {
        findNavController().navigate(navDirections)
    }
}

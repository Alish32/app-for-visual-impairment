package com.spartons.androidml.workers

import android.content.Context
import androidx.work.CoroutineWorker
import androidx.work.WorkerParameters
import com.google.firebase.ml.common.modeldownload.FirebaseModelDownloadConditions
import com.google.firebase.ml.common.modeldownload.FirebaseModelManager
import com.google.firebase.ml.vision.automl.FirebaseAutoMLRemoteModel
import com.spartons.androidml.extensions.await

class ImageDownloadWorker(content: Context, workerParams: WorkerParameters) :
    CoroutineWorker(content, workerParams) {

    private val remoteModel = FirebaseAutoMLRemoteModel.Builder("persons").build()
    private val remoteModelConditions = FirebaseModelDownloadConditions.Builder()
        .build()
    private val remoteModelManager = FirebaseModelManager.getInstance()

    override suspend fun doWork(): Result {
        val isModelDownload = remoteModelManager.isModelDownloaded(remoteModel).await()
        if (isModelDownload) return Result.success()
        return try {
            remoteModelManager.download(remoteModel,remoteModelConditions).await()
            Result.success()
        }catch (e: Exception){
            e.printStackTrace()
            Result.retry()
        }
    }
}
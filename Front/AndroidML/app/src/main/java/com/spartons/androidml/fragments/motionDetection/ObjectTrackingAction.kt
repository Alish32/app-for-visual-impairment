package com.spartons.androidml.fragments.motionDetection

interface ObjectTrackingAction {

    fun onTrackingStart()
}
package com.spartons.androidml.fragments.motionDetection

import android.os.Bundle
import android.speech.tts.TextToSpeech
import android.util.Log
import android.view.ViewGroup
import androidx.lifecycle.lifecycleScope
import com.google.firebase.ml.vision.objects.FirebaseVisionObjectDetectorOptions
import com.spartons.androidml.R
import com.spartons.androidml.cameraUtil.CameraSource
import com.spartons.androidml.cameraUtil.CameraSourcePreview
import com.spartons.androidml.cameraUtil.GraphicOverlay
import com.spartons.androidml.fragments.BaseFragment
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import java.io.IOException
import java.util.*
import java.util.concurrent.atomic.AtomicInteger
import kotlin.math.log

class MotionDetectionFragment : BaseFragment(), ObjectTrackingAction {

    private val textToSpeechCallback = TextToSpeech.OnInitListener {}
    private val atomicInteger = AtomicInteger()
    private val textToSpeech by lazy {
        TextToSpeech(requireContext(), textToSpeechCallback).apply { language = Locale.US }
    }


    private lateinit var fireFaceOverlay: GraphicOverlay
    private lateinit var firePreview: CameraSourcePreview

    private val cameraSource = lazy {
        CameraSource(requireActivity(), fireFaceOverlay).also {
            val objectDetectorOptions = FirebaseVisionObjectDetectorOptions.Builder()
                .setDetectorMode(FirebaseVisionObjectDetectorOptions.STREAM_MODE)
                .enableClassification().build()
            it.setMachineLearningFrameProcessor(
                ObjectDetectorProcessor(objectDetectorOptions, this)
            )
        }
    }

    override fun getLayoutResId() = R.layout.fragment_motion_detection

    override fun inOnCreateView(mRootView: ViewGroup, savedInstanceState: Bundle?) {
        fireFaceOverlay = mRootView.findViewById(R.id.fireFaceOverlay)
        firePreview = mRootView.findViewById(R.id.firePreview)
    }

    private fun startCameraSource() {
        cameraSource.let {
            try {
                firePreview.start(cameraSource.value, fireFaceOverlay)
            } catch (e: IOException) {
                e.printStackTrace()
                cameraSource.value.release()
            }
        }
    }

    override fun onResume() {
        super.onResume()
        Log.d("MOTIONDETECTION","ONRESUME CALLED")
        startCameraSource()
    }

    override fun onPause() {
        super.onPause()
        firePreview.stop()
    }

    override fun onStop() {
        super.onStop()
        textToSpeech.speak(
            "", TextToSpeech.QUEUE_FLUSH,
            null, atomicInteger.incrementAndGet().toString()
        )
    }

    override fun onDestroy() {
        super.onDestroy()
        cameraSource.value.release()
        textToSpeech.shutdown()
    }

    override fun onTrackingStart() {
        if (textToSpeech.isSpeaking.not()) {
            lifecycleScope.launch { speakText("Object is locked") }
        }
    }

    private suspend fun speakText(msg: String, id: Int? = null) = withContext(Dispatchers.Main) {
        val utteranceId = id ?: atomicInteger.incrementAndGet()
        textToSpeech.speak(
            msg, TextToSpeech.QUEUE_ADD,
            null, utteranceId.toString()
        )
    }
}
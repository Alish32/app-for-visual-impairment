package com.spartons.androidml.extensions

import android.content.Context
import android.content.pm.PackageManager
import android.hardware.SensorManager
import android.hardware.camera2.CameraManager
import android.os.Build
import android.widget.Toast
import androidx.core.content.ContextCompat

val Context.sensorManager get() = getSystemService(Context.SENSOR_SERVICE) as? SensorManager

fun Context.isHasPermission(vararg permissions: String): Boolean {
    return if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M)
        permissions.all { singlePermission ->
            ContextCompat.checkSelfPermission(
                this, singlePermission
            ) == PackageManager.PERMISSION_GRANTED
        }
    else true
}

val Context.cameraManager get() = getSystemService(Context.CAMERA_SERVICE) as CameraManager

fun Context.toast(msg: String) {
    Toast.makeText(this, msg, Toast.LENGTH_SHORT).show()
}
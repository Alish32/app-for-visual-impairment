package com.spartons.androidml.fragments.motionDetection

import android.graphics.Bitmap
import android.util.Log

import com.google.android.gms.tasks.Task
import com.google.firebase.ml.vision.FirebaseVision
import com.google.firebase.ml.vision.common.FirebaseVisionImage
import com.google.firebase.ml.vision.objects.FirebaseVisionObject
import com.google.firebase.ml.vision.objects.FirebaseVisionObjectDetector
import com.google.firebase.ml.vision.objects.FirebaseVisionObjectDetectorOptions
import com.spartons.androidml.cameraUtil.CameraImageGraphic
import com.spartons.androidml.cameraUtil.FrameMetadata
import com.spartons.androidml.cameraUtil.GraphicOverlay
import com.spartons.androidml.cameraUtil.VisionProcessorBase

import java.io.IOException

/** A processor to run object detector.  */
class ObjectDetectorProcessor(
    options: FirebaseVisionObjectDetectorOptions,
    private val objectTrackingAction: ObjectTrackingAction
) :
    VisionProcessorBase<List<FirebaseVisionObject>>() {

    private var previousObjectDetectionId: Int = -1

    private val detector: FirebaseVisionObjectDetector =
        FirebaseVision.getInstance().getOnDeviceObjectDetector(options)

    override fun stop() {
        super.stop()
        try {
            detector.close()
            Log.e(TAG, "close")
        } catch (e: IOException) {
            Log.e(TAG, "Exception thrown while trying to close object detector: $e")
        }
    }

    override fun detectInImage(image: FirebaseVisionImage): Task<List<FirebaseVisionObject>> {
        return detector.processImage(image)
    }

    override fun onSuccess(
        originalCameraImage: Bitmap?,
        results: List<FirebaseVisionObject>,
        frameMetadata: FrameMetadata,
        graphicOverlay: GraphicOverlay
    ) {
        graphicOverlay.clear()
        if (originalCameraImage != null) {
            val imageGraphic = CameraImageGraphic(graphicOverlay, originalCameraImage)
            graphicOverlay.add(imageGraphic)
        }
        if (results.isNotEmpty()) {
            val currentTrackingId = results.component1().trackingId
            if (currentTrackingId != null && previousObjectDetectionId != currentTrackingId) {
                objectTrackingAction.onTrackingStart()
                previousObjectDetectionId = currentTrackingId
            }
        }

        for (visionObject in results) {
            val objectGraphic = ObjectGraphic(graphicOverlay, visionObject)
            graphicOverlay.add(objectGraphic)
        }
        graphicOverlay.postInvalidate()
    }

    override fun onFailure(e: Exception) {
        Log.e(TAG, "Object detection failed $e")
    }

    companion object {
        private const val TAG = "ObjectDetectorProcessor"
    }
}

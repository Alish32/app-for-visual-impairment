package com.spartons.androidml.fragments.lightDetection

import android.hardware.Sensor
import android.hardware.SensorEvent
import android.hardware.SensorEventListener
import android.hardware.SensorManager
import android.media.AudioManager
import android.media.ToneGenerator
import android.os.Bundle
import android.speech.tts.TextToSpeech
import android.view.ViewGroup
import androidx.lifecycle.lifecycleScope
import com.spartons.androidml.R
import com.spartons.androidml.extensions.sensorManager
import com.spartons.androidml.fragments.BaseFragment
import kotlinx.android.synthetic.main.fragment_light_detection.*
import kotlinx.coroutines.*
import java.util.*
import java.util.concurrent.atomic.AtomicInteger


class LightDetectionFragment : BaseFragment() {

    private companion object {
        private const val FULL_MOON = 0.108
        private const val TWILIGHT = 10.8
        private const val VERY_DARK_DAY = 107
        private const val INITIAL_LIGHT_DETECTION_SPEAK_DELAY = 2000L
        private const val NORMAL_LIGHT_DETECTION_SPEAK_DELAY = 5000L
    }

    private fun getLightDetectionString(value: Float): String {
        //speakText("Light Detection")
        return when {
            value <= FULL_MOON -> "No light"
            //value > FULL_MOON && value <= TWILIGHT -> "Low light"
            //value > TWILIGHT && value <= VERY_DARK_DAY -> "Normal light"
            //value > VERY_DARK_DAY -> "Full light"
            else -> "Light detection is not found"
        }
    }

    private fun getLightDetectionFrequency(value: Float): Int {
        return when {
            value <= FULL_MOON -> 0
            value > 1 && value <= 50 -> 1000
            value > 51 && value <= 150 -> 900
            value > 151 && value <= 250 -> 800
            value > 251 && value <= 300 -> 750
            value > 351 && value <= 350 -> 700
            value > 451 && value <= 450 -> 600
            value > 551 && value <= 550 -> 500
            value > 651 && value <= 650 -> 400
            value > 751 && value <= 750 -> 300
            value > 851 && value <= 850 -> 200
            value > 951 && value <= 950 -> 100
            value > 1001 -> 100
            else -> 0
        }
    }

    private val atomicInteger = AtomicInteger()
    private val textToSpeechCallback = TextToSpeech.OnInitListener {}

    private val textToSpeech by lazy {
        TextToSpeech(requireContext(), textToSpeechCallback).apply { language = Locale.US }
    }

    private val lightSensor by lazy {
        requireContext().sensorManager?.getDefaultSensor(Sensor.TYPE_LIGHT)
    }
    val toneGen1 = ToneGenerator(AudioManager.STREAM_MUSIC, 100)
    var aa = 0;
    private val sensorEventListener = object : SensorEventListener {
        override fun onAccuracyChanged(p0: Sensor?, p1: Int) = Unit

        override fun onSensorChanged(event: SensorEvent?) {
            event ?: return
            recentLightDetectionValue = event.values[0]
            my_light_text_view.text = recentLightDetectionValue.toString()
            aa = event.values[0].toInt()

        }
    }

    private var recentLightDetectionValue = 0f

    override fun getLayoutResId() = R.layout.fragment_light_detection

    override fun inOnCreateView(mRootView: ViewGroup, savedInstanceState: Bundle?) = Unit
    override fun onResume() {
        super.onResume()
        requireContext().sensorManager?.registerListener(
            sensorEventListener, lightSensor,
            SensorManager.SENSOR_DELAY_UI
        )
        lifecycleScope.launch(Dispatchers.IO) {
            // for to remove the latency in first speak
            speakText("")
            delay(INITIAL_LIGHT_DETECTION_SPEAK_DELAY)
            while (isActive) {
                val lightDetectionString = getLightDetectionString(recentLightDetectionValue)
                if (aa <= 0) {
                    speakText(lightDetectionString)
                    delay(5000)
                } else {
                    toneGen1.startTone(ToneGenerator.TONE_DTMF_0, aa)
                    delay(1000)
                }
                //toneGen1.startTone(ToneGenerator.TONE_CDMA_HIGH_L, aa - 1)
            }
        }
    }

    private fun speakText(text: String) {
        textToSpeech.speak(
            text, TextToSpeech.QUEUE_ADD,
            null, atomicInteger.getAndIncrement().toString()
        )
    }

    override fun onStop() {
        super.onStop()
        requireContext().sensorManager?.unregisterListener(sensorEventListener)
        lifecycleScope.coroutineContext.cancelChildren()
        textToSpeech.speak(
            "", TextToSpeech.QUEUE_FLUSH,
            null, atomicInteger.incrementAndGet().toString()
        )
    }

    override fun onDestroy() {
        super.onDestroy()
        textToSpeech.shutdown()
    }
}
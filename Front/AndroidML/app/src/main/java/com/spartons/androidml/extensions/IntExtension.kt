package com.spartons.androidml.extensions

import com.google.firebase.ml.vision.objects.FirebaseVisionObject.*

fun Int.splitRgbValue(): List<Int> {
    val red: Int = this shr 16 and 0xFF
    val green: Int = this shr 8 and 0xFF
    val blue: Int = this and 0xFF
    return listOf(red, green, blue)
}

fun Int.getFirebaseVisionObjectCategoryName(): String {
    return when (this) {
        CATEGORY_HOME_GOOD -> "Home Good things are detected"
        CATEGORY_FASHION_GOOD -> "Fashion Good are detected"
        CATEGORY_FOOD -> "Food Item detected"
        CATEGORY_PLACE -> "A place detected"
        CATEGORY_PLANT -> "A Plant detected"
        else -> "Unknown object detected"
    }
}
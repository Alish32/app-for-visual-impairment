package com.spartons.androidml.fragments.selection

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.spartons.androidml.R
import kotlinx.android.extensions.LayoutContainer

class SelectionAdapter(private val callback: SelectionAdapterActionHandler) :
    ListAdapter<SelectType, SelectionAdapterViewHolder>(SelectionAdapterDiffCallback) {

    private companion object {
        private val items = SelectType.values().toList()
    }

    init {
        submitList(items)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) =
        SelectionAdapterViewHolder.create(parent, callback)

    override fun onBindViewHolder(holder: SelectionAdapterViewHolder, position: Int) {
        holder.bind(getItem(position))
    }
}

internal object SelectionAdapterDiffCallback : DiffUtil.ItemCallback<SelectType>() {

    override fun areItemsTheSame(oldItem: SelectType, newItem: SelectType) = oldItem == newItem

    override fun areContentsTheSame(oldItem: SelectType, newItem: SelectType) = oldItem == newItem

}

class SelectionAdapterViewHolder(
    override val containerView: View,
    private val callback: SelectionAdapterActionHandler
) : RecyclerView.ViewHolder(containerView), LayoutContainer {

    companion object {
        fun create(parent: ViewGroup, callback: SelectionAdapterActionHandler) =
            SelectionAdapterViewHolder(
                LayoutInflater.from(parent.context).inflate(
                    R.layout.selection_single_type,
                    parent, false
                ), callback
            )
    }

    fun bind(item: SelectType) {
        itemView.findViewById<Button>(R.id.selection_single_type_button).run {
            text = item.value
            setOnClickListener { callback.itemSelected(item) }
        }
    }
}
package com.spartons.androidml

import org.junit.Test

/**
 * Example local unit test, which will execute on the development machine (host).
 *
 * See [testing documentation](http://d.android.com/tools/testing).
 */
class ExampleUnitTest {

    private companion object {
        private const val IMAGE_DETECTION_OBJECT_CONFIDENCE = 0.6
    }

    private val flagList = mutableListOf<Boolean>()
    private val confidences = listOf(0.1, 0.6)

    @Test
    fun addition_isCorrect() {
        for ((index, confidence) in confidences.withIndex()) {
            println(index)
            flagList.add(confidence >= IMAGE_DETECTION_OBJECT_CONFIDENCE)
        }

        val flag = flagList.all { it.not() }
        println(flag)
    }
}

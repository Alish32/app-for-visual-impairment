# README #

# Step 1: 
Open Android Studio. After that you Click on “Open an existing Android Studio project”.

# Step 2: 
Select the File location where the project is located and then Click OK.

# Step 3: 
Click on Projects >> and choose “MainActivity.kt” file.

# Step 4: Enable USB debugging
To do this follow these steps:
	On your phone (or tablet) go to Settings=> About Phone
	Tap Build Number 7 times, after 7th time it will say You are now a developer.
	You will notice Developer’s Options are now available in setting
	Go to the Developer option and enable USB debugging and click OK

# Step 5: 
No you can run your Android app. Right click on the app and click Run.
Or simply select run option from the tool bar menu

# Step 6: 
A window Select Deployment Target will appear, and a list of available devices will appear.
Choose your device and click OK.
Android Studio will run your application in your Android device.
from django.contrib import admin
from django.urls import path
from .views import save_known_face_image
from .views import save_and_delete_unknown_face_image
from .views import recognize_person
from .views import extract_face_from_image
from .views import gender_and_age
from .views import run_paralel


urlpatterns = [
    path('gender_and_age/', gender_and_age),
    path('recognize_person/', recognize_person),
    path('save_known_face_image/', save_known_face_image),
    path('save_and_delete_unknown_face_image/', save_and_delete_unknown_face_image),
    path('extract_face_from_image/', extract_face_from_image),
    path('run_paralel/', run_paralel),

]
